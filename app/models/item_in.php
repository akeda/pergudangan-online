<?php
class ItemIn extends AppModel {
    var $validate = array(
        'item_id' => array(
            'rule' => array('vItem'),
            'message' => 'Choose based on available options'
        ),
        'total' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'numeric',
            'message' => 'This field cannot be left blank and must be numeric'
        ),
        'date_in' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'This field cannot be left blank and must be date'
        )
    );

    var $belongsTo = array(
        'Item',
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
    var $cacheQueries = true;
    
    function afterFind($results) {
        
        if ( isset($results[0]['Item']) && !empty($results[0]['Item']) ) {
            // get list of item's unit
            // TODO: caching this one
            $units = $this->Item->Unit->find('list');
            
            foreach ( $results as $key => $result ) {
                $results[$key]['Item']['item_name'] = $units[$result['Item']['unit_id']];
            }
        }
        
        return $results;
    }
    
    function getStock() {
    }
    
    function vItem($field) {
        $exist = $this->Item->find('count', array(
            'conditions' => array(
                'Item.id' => $field["item_id"]
            ),
            'recursive' => -1)
        );
        return $exist > 0;
    }
    
    function getTotal($item_id, $periode = null) {
        $conditions = array(
            'ItemIn.item_id' => $item_id
        );
        $fields = array('ItemIn.total', 'ItemIn.created');
        
        if ( !is_null($periode) ) {
            /*
            $conditions['or'] = array(
                'ItemIn.date_in <' => $periode,
                'ItemIn.date_in' => $periode,
            );*/
            $conditions['ItemIn.date_in <='] = $periode;
            $fields[] = 'ItemIn.date_in';
        }
        
        $totals = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'contain' => array(),
            'order' => 'ItemIn.created ASC'
        ));
        
        if ( !is_null($periode) ) {
            return $totals;
        }
        
        $result = 0;
        foreach ($totals as $total) {
            $result += $total['ItemIn']['total'];
        }
        return $result;
    }
    
    function getTotalExt($item_id, $periode) {
        $conditions = array(
            'ItemIn.item_id' => $item_id,
            'ItemIn.date_in <=' => $periode
        );
        $fields = array(
            'ItemIn.created',
            'ItemIn.date_in',
            'SUM(ItemIn.total) as total'
        );
        
        return $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields,
            'contain' => array(),
            'group' => 'ItemIn.date_in',
            'order' => 'ItemIn.date_in ASC'
        ));
    }
}
?>