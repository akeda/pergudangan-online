<?php
class ItemOut extends AppModel {
    var $validate = array(
        'item_id' => array(
            'rule' => array('vItem'),
            'message' => 'Choose based on available options'
        ),
        'total' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'numeric',
            'message' => 'This field cannot be left blank and must be numeric'
        ),
        'date_out' => array(
            'required' => true,
            'allowEmpty' => false,
            'rule' => 'date',
            'message' => 'This field cannot be left blank and must be date'
        ),
        
    );

    var $belongsTo = array(
        'Item',
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
    var $cacheQueries = true;
    
    function beforeSave() {
        parent::beforeSave();
        
        // make sure only when edit action
        // happens (approving)
        if ( isset($this->data[$this->name]['modified']) ) {
            if ( $this->data[$this->name]['approved'] ) { // approved
                ClassRegistry::init('ItemIn');
                $ItemIn = new ItemIn;
                $item_id = $this->data[$this->name]['item_id'];
                
                $stok = $ItemIn->getTotal($item_id) - $this->getTotal($item_id);
                $available = $stok - $this->data[$this->name]['total_approved'];
                if ( $available < 0 ) {
                    $this->validationErrors['approved'] = 'Jumlah yang diminta melebihi stok. Harap laporkan admin bahwa stok tinggal ' . $stok . '.';
                    return false;
                } else { // ok, approved
                
                }
            }
        }
        
        return true;
    }
    
    function vItem($field) {
        $exist = $this->Item->find('count', array(
            'conditions' => array(
                'Item.id' => $field["item_id"]
            ),
            'recursive' => -1)
        );
        return $exist > 0;
    }

/**
 * Get total
 * @param int $item_id
 * @periode string datetime of created
 */
    function getTotal($item_id, $periode = null) {
        $conditions = array(
            'ItemOut.item_id' => $item_id,
            'ItemOut.approved' => 1
        );
        $fields = array('ItemOut.total_approved');
        
        if ( !is_null($periode) ) {
            $conditions['DATE(ItemOut.date_approved) <='] = $periode;
            $fields[] = 'ItemOut.date_approved';
        }
        
        $totals = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields
        ));
        
        if ( !is_null($periode) ) {
            $result = 0;
            foreach ( $totals as $total ) {
                $result += $total['ItemOut']['total_approved'];
            }
            return $result;
        }
        
        $result = 0;
        foreach ($totals as $total) {
            $result += $total['ItemOut']['total_approved'];
        }
        return $result;
    }
    
    function getTotalOutExcludeLast($item_id, $periode) {
        $conditions = array(
            'ItemOut.item_id' => $item_id,
            'ItemOut.approved' => 1,
            'DATE(ItemOut.date_approved) <' => $periode
        );
        $fields = array('ItemOut.total_approved', 'ItemOut.date_approved');
        
        $totals = $this->find('all', array(
            'conditions' => $conditions,
            'fields' => $fields
        ));
                
        $result = 0;
        foreach ( $totals as $total ) {
            $result += $total['ItemOut']['total_approved'];
        }
        return $result;
    }
    
    function getByDate($item_id, $date_from, $date_to) {
        return $this->find('all', array(
            'conditions' => array(
                'DATE(ItemOut.date_approved) >=' => $date_from,
                'DATE(ItemOut.date_approved) <' => $date_to,
                'ItemOut.item_id' => $item_id,
                'ItemOut.approved' => 1
            ),
            'fields' => array(
                'ItemOut.date_approved', 'ItemOut.total_approved',
                'User.name'
            ),
            'order' => 'ItemOut.date_approved ASC, ItemOut.created ASC'
        ));
    }
}
?>