<?php
class Item extends AppModel {
    var $validate = array(
        'name' => array(
            'required' => array(
                'required' => true,
                'allowEmpty' => false,
                'rule' => '/^\w+[\w\s]?/',
                'message' => 'This field cannot be left blank and must be alphanumeric'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 50),
                'message' => 'Maximum characters is 50 characters'
            )
        ),
        'code' => array(
            'required' => array(    
                'required' => true,
                'allowEmpty' => false,
                'rule' => '|^[a-zA-Z0-9\.]{1,10}$|',
                'message' => 'This field cannot be left blank and must be alphanumeric or .'
            ),
            'maxlength' => array(
                'rule' => array('maxLength', 20),
                'message' => 'Maximum character is 20 characters'
            ),
            'unique' => array(
                'rule' => array('unique', 'code'),
                'message' => 'Code already exists'
            )
        ),
        'unit_id' => array(
            'rule' => array('vUnit'),
            'message' => 'Choose based on available option'
        )
    );
    
    var $belongsTo = array(
        'Unit',
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'created_by',
            'fields' => array('id', 'name')
        )
    );
    
    function vUnit($field) {
        return $this->Unit->find('count', array(
            'conditions' => array(
                'Unit.id' => $field["unit_id"]
            ),
            'recursive' => -1
        )) > 0;
    }
}
?>