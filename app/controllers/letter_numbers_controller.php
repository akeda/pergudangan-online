<?php
class LetterNumbersController extends AppController {
    var $pageTitle = 'Letter Numbers';
    
    function index() {
        $this->LetterNumber->Behaviors->attach('Containable');
        $this->LetterNumber->contain('User');
        $this->paginate['LetterNumber'] = array(
            'contain' => array('User')
        );
        $this->set('getStock', "var getStock = '" . $this->webroot . $this->params['controller'] . "/getStock/';");
        parent::index();
    }
    
    function getStock($id = null) {
        $this->layout = 'ajax';
        
        if ( Configure::read('debug') > 0 ) {
            Configure::write('debug', 0);
        }
        
        if ( !$id ) {
            $this->set('result', '');
	    }
        
        $result = $this->LetterNumber->getStock($id);
        if ( !isset($result[0][0]) && empty($result[0][0]) ) {
            $result = '';
        } else {
            $result = $result[0][0]['stock'];
        }
	    $this->set('result', $result);
    }
}
?>