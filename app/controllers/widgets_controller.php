<?php
class WidgetsController extends AppController {
    var $uses = null;
    
    public function summary_items() {
        // turn off debugging and uses ajax layout
        Configure::write('debug', 0);
        $this->layout = 'ajax';
        
        App::import('Model', array('ItemIn', 'ItemOut'));
        $ItemIn = new ItemIn();
        $ItemOut = new ItemOut();
        $items = $ItemIn->Item->find('all', array(
            'fields' => array('Item.id', 'Item.name', 'Unit.name'),
            'order' => 'Item.name ASC'
        ));
        $total_items = count($items);
        $zero_stocks = $available_stocks = 0;
        foreach ($items as $key => $item) {
            $items[$key]['Item']['penerimaan'] = $ItemIn->getTotal( $item['Item']['id'] );
            $items[$key]['Item']['pengeluaran'] = $ItemOut->getTotal( $item['Item']['id'] );
            $items[$key]['Item']['stok'] = $items[$key]['Item']['penerimaan'] - $items[$key]['Item']['pengeluaran'];

            if ( $items[$key]['Item']['stok'] <= 0 ) {
                $zero_stocks++;
            } else {
                $available_stocks++;
            }
        }
        
        $this->set('total_items', $total_items);
        $this->set('zero_stocks', $zero_stocks);
        $this->set('available_stocks', $available_stocks);
    }
}
?>
