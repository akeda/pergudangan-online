<?php
class ItemInsController extends AppController {
    var $pageTitle = 'Input Barang';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals();
        parent::edit($id);
    }
    
    function getSatuan($item_id = null) {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        $result = $this->ItemIn->Item->find('first', array(
            'conditions' => array(
                'Item.id' => $item_id
            ),
            'fields' => array('Unit.name')
        ));
        
        $this->set('result', $result);
    }
    
    function __setAdditionals() {
        $items = $this->ItemIn->Item->find('list', array(
            'order' => array('Item.name ASC')
        ));
        $this->set('items', $items);
        
        $this->set('getSatuan', "var getSatuan = '" . $this->webroot . $this->params['controller'] . "/getSatuan/';");
    }
}
?>