<?php
class LetterOutsController extends AppController {
    var $pageTitle = 'Request Letter No';
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function view($id = null) {
        if (!$id) {
            $this->Session->setFlash(__('Invalid parameter', true), 'error');
			$this->__redirect();
        } else {
            $this->data = $this->{$this->modelName}->find('first', array($this->modelName . '.id' => $id));
            
            if ( empty($this->data) ) {
                $this->Session->setFlash(__('Invalid parameter', true), 'error');
			    $this->__redirect();
            }
            $this->__setAdditionals();
        }
        
        
    }
    
    function __setAdditionals() {
        $unit_codes = $this->LetterOut->UnitCode->find('list', array(
            'order' => array('UnitCode.name ASC')
        ));
        $term_codes = $this->LetterOut->TermCode->find('list', array(
            'order' => array('TermCode.name ASC')
        ));
        $this->set('unit_codes', $unit_codes);
        $this->set('term_codes', $term_codes);
    }
}
?>