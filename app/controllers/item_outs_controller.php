<?php
class ItemOutsController extends AppController {
    var $pageTitle = 'Daftar Pengebon Barang';
    
    function index() {
        $this->paginate['order'] = 'ItemOut.created DESC';
        parent::index();
    }
    
    function add() {
        $this->__setAdditionals();
        parent::add();
    }
    
    function edit($id = null) {
        $this->__setAdditionals($id);
        parent::edit($id);
        
        if ( !isset($this->data['Item']) ) {
            $item = $this->ItemOut->find('first', array(
                'conditions' => array('ItemOut.id' => $id)
            ));
            $this->data = $item;
        }
    }
    
    function getSatuan($item_id = null) {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        $result = $this->ItemOut->Item->find('first', array(
            'conditions' => array(
                'Item.id' => $item_id
            ),
            'fields' => array('Unit.name')
        ));
        
        $this->set('result', $result);
    }
    
    function get_new_status() {
        $this->layout = 'ajax';
        Configure::write('debug', 0);
        
        $result = $this->ItemOut->find('count', array(
            'conditions' => array(
                'ItemOut.approved' => 0
            ),
            'recursive' => -1
        ));
        
        $this->set('result', $result);
    }
    
    function __setAdditionals() {
        $items = $this->ItemOut->Item->find('list', array(
        'order' => array('Item.name ASC')
        ));
        $this->set('items', $items);
        
        $this->set('getSatuan', "var getSatuan = '" . $this->webroot . $this->params['controller'] . "/getSatuan/';");
    }
}
?>