<div class="<?php echo $this->params['controller']; ?> index">
<?php 
    echo $this->element('tablegrid', array(
            "fields" => array(
                "code" => array(
                    'title' => __('Code', true),
                    'sortable' => true
                ),
                "name" => array(
                    'title' => __('Name', true),
                    'sortable' => true
                ), 
                "unit_id" => array(
                    'title' => __('Unit', true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __('Created By', true),
                    'sortable' => false
                )
            ),
            "assoc" => array(
                'unit_id' => array(
                    'model' => 'Unit',
                    'field' => 'name'
                ),
                'created_by' => array(
                    'model' => 'User',
                    'field' => 'name'
                )
            ),
            "editable"  => 'name'
    ));
?>
</div>
