<!-- begin tablegrid-head -->
<div class="tablegrid-head">
    <div class="module-head">
        <div class="module-head-c">
            <h2><?php echo isset($this->pageTitle) ? __($this->pageTitle, true) : __($modelName, true); ?></h2>
            <div class="module-tools">
                <?php if ( $module_permission['operations']['print']): ?>
                <a href='#' class="media print"><span>Print</span></a> | 
                <?php endif;?>
                <?php if ( $module_permission['operations']['csv']): ?>
                <a href='#' class="media csv"><span>Save as csv</span></a> |
                <?php endif;?>
                <?php if ( $module_permission['operations']['pdf']): ?>
                <a href='#' class="media pdf"><span>Save as pdf</span></a>
                <?php endif;?>
            </div>
        </div>
    </div>
    
    <br class="clear" />
    <!-- filter -->
    <?php if (isset($filter)):?>
    <div id="filter-box">
        <form accept-charset="UNKNOWN" enctype="application/x-www-form-urlencoded" method="get" name="search-form" id="search-form">
        <table cellpadding="0" cellspacing="0" id="tablefilter">
            <tr>
                <td colspan="2" class="filter-title"><?php echo $html->image('search.png', array('alt'=>'search')) ?>&nbsp;&nbsp;Filter</td>
            </tr>
            <?php 
                $i = 0;
                foreach ( $filter as $input_field => $label ): 
             ?>
            <tr>
                <td><?php __($label); ?></td>
                <td>
                    <input class="inline" type="text" name="<?php echo "$input_field"; ?>" value="<?php echo isset(${$input_field."Value"}) ? ${$input_field."Value"} : ''; ?>" />
                    <?php 
                        if ( $i == (sizeof($filter) - 1) ) {
                            echo $form->submit('Search', array('div'=>false, 'name' => 'search')) . "&nbsp;";
                            echo $html->link(__('Clear', true), array('action'=>'index'), array('class'=>'clear')) . "&nbsp;";
                        }
                        $i++;
                    ?>
                 </td>
            </tr>
            <?php endforeach; ?>
        </table>
        </form>
    <!-- filter eof --> 
    </div>
    <?php endif;?>
<!-- tablegrid-head eof -->    
</div>

<div class="notice">
Item dibawah ini adalah item yang memiliki kesalahan waktu keluar barang yang mendahului waktu stok barang pertama kali
masuk (kolom <strong>Barang Keluar Sebelum Stok Pertama</strong>) atau juga kesalahan yang menyebabkan terjadinya stok
negatif pada waktu tertentu (kolom <strong>Stok Negative</strong>). Stok negative terjadi karena pada waktu meng-<em>approve</em>
barang keluar, stok di bawah minimum dari jumlah yang diajukan, lalu saat stok baru masuk dan proses <em>approval</em> berhasil
tanggal approval masih tanggal lama sebelum stok barang terbaru masuk. Untuk memperbaiki barang keluar sebelum stok pertama, klik 
<strong>Fix Barang Keluar</strong>, dengan ini semua item akan di-<em>update</em> tanggal approvalnya ke tanggal
stok pertama kali masuk. Untuk memperbaiki stok negatif, sesuaikan tanggal dengan tanggal yang tersedia lalu 
klik <strong>Fix Negative Stock</strong>. Cek Laporan Stok Barang dengan Kartu Stok Barang untuk memastikannya.
</div>

<!-- begin tablegrid -->
<table cellpadding="0" cellspacing="0" class="tablegrid" id="tablegrid_<?php echo $this->params['controller'];?>">
<thead>
<tr>
    <th>Nama Barang</th>
    <th>Kode Barang</th>
    <th>Jumlah Stok</th>
    <th>Barang Keluar Sebelum Stok Pertama</th>
    <th>Stok Negative</th>
    <th>Actions</th>
</tr>
</thead>
<tfoot>
<tr>
    <th>Nama Barang</th>
    <th>Kode Barang</th>
    <th>Jumlah Stok</th>
    <th>Barang Keluar Sebelum Stok Pertama</th>
    <th>Stok Negative</th>
    <th>Actions</th>
</tr>
</tfoot>
<tbody>
<?php
$i = 0;
if (empty($records)) {
    echo '<tr><td colspan="5" class="noRecord">' . __('No Records', true) . '</td></tr>';
}
foreach ($records as $record):
    $class = null;
    if ($i++ % 2 == 0) {
        $class = ' class="altrow"';
    }
?>
    <tr<?php echo $class;?> rel="r<?php echo $record["$modelName"]["id"];?>">
        <td><?php echo $record['Item']['name'];?></td>
        <td><?php echo $record['Item']['code'];?></td>
        <td><?php echo $record['Item']['stok']. ' ' . $record['Unit']['name'];?></td>
        <td class="below_first_stock_first_in">
        <?php
            if ( !empty($record['Item']['below_first_stock_first_in']) ) {
                echo "<span style=\"color: green\">{$record['Item']['stock_first_in']}</span><br />";
                foreach ( $record['Item']['below_first_stock_first_in'] as $k => $b ) {
                    echo "<span style=\"color: red\">{$b['ItemOut']['date_approved']}: {$b['ItemOut']['total_approved']}</span>";
                    
                    if ( isset($record['Item']['below_first_stock_first_in'][$k+1]) ) {
                        echo "<br />";
                    }
                }
                
                
                echo '<input type="hidden" 
                             name="data[items]['.$record['Item']['id'].'][item_id]" 
                             value="'.$record['Item']['id'].'"/>';
                echo '<input type="hidden" 
                             name="data[items]['.$record['Item']['id'].'][stock_first_in]" 
                             value="'.$record['Item']['stock_first_in'].'" />';
            } else {
                echo '-';
            }
        ?>    
        </td>
        <td style="min-width: 250px" class="negative_stock">
        <?php
            if ( !empty($record['Item']['negative_stock']) ) {
                foreach ( $record['Item']['negative_stock'] as $k => $n ) {
                    if ( is_string($k) ) {
                        echo "<span style=\"color: green; display: inline-block; width: 150px;\">$k: $n</span><br />";
                        continue;
                    }
                    
                    echo "<span style=\"color: red; display: inline-block; width: 150px;\">{$n['date_approved']}: {$n['total_approved']}</span>";
                    echo '<select style="display: inline-block"
                                  name="data[items]['.$n['id'].']"
                                  rel="'.$n['id'].'"
                                  class="select_date_approved">';
                    foreach ($n['select_date_approved'] as $date_approved) {
                        echo '<option value="' . $date_approved . '">' . $date_approved . '</option>';
                    }
                    echo '</select><br />';
                }
            } else {
                echo '-';
            }
        ?>    
        </td>
        <td>
            <?php if ( !empty($record['Item']['below_first_stock_first_in']) ): ?>
                <a href="#" class="act_first_stock_in">Fix Barang Keluar</a>
            <?php else: ?>
                <span style="color: #999;">Fix Barang Keluar</span>
            <?php endif;?>
            |
            <?php if ( !empty($record['Item']['negative_stock']) ): ?>
                <a href="#" class="act_negative_stock">Fix Negative Stock</a>
            <?php else: ?>
                <span style="color: #999;">Fix Negative Stock</span>
            <?php endif;?>
        </td>
    </tr>
<?php endforeach; ?>
</tbody>
</table>
<!-- end table grid -->
<form id="below_first_stock_first_in" method="post" style="display: none"></form>
<form id="negative_stock" method="post" style="display: none"></form>
<script type="text/javascript">
$(function(){
    
    /* on first load create hidden input to hold selected value of .select_date_approved */
    $('.select_date_approved').each(function() {
        var name = $(this).attr('name');
        var id = $(this).attr('rel');
        $(this).after('<input type="hidden" id="i'+id+'" name="'+name+'" value="'+this.value+'" />');
    /* on change update the hidden input */
    }).change(function() {
        var id = $(this).attr('rel');
        $('#i'+id).val( this.value );
    });
    
    $('.act_first_stock_in').click(function(e) {
       var tr = $(this).parent().parent();
       
       var input = $('td.below_first_stock_first_in', tr).clone();
       $('span', input).remove();
       input.append('<input type="hidden" name="data[act]" value="first_stock_in" />');
       input = input.html()
       
       $('#below_first_stock_first_in').html(input).submit();
       
       e.preventDefault();
    });
    
    $('.act_negative_stock').click(function(e) {
        var tr = $(this).parent().parent();
        
        var input = $('td.negative_stock', tr).clone();
        $('span', input).remove();
        $('select', input).remove();
        input.append('<input type="hidden" name="data[act]" value="negative_stock" />');
        input = input.html();
       
        $('#negative_stock').html(input).submit();
       
        e.preventDefault();
    });
});
</script>