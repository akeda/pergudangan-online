<div class="<?php echo $this->params['controller']; ?> index">
<?php
    echo $this->element('tablegrid',
        array("fields" => array(
                "item_id" => array(
                    'title' => __("Nama Barang", true),
                    'sortable' => true
                ),
                "item_id2" => array(
                    'title' => __("Kode Barang", true),
                    'sortable' => true
                ),
                "total" => array(
                    'title' => __("Jumlah", true),
                    'sortable' => false
                ),
                "item_name" => array(
                    'title' => __("Satuan", true),
                    'sortable' => false
                ),
                "date_in" => array(
                    'title' => __("Tanggal Terima Barang", true),
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Diinput oleh", true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __("Waktu input", true),
                    'sortable' => true
                )
              ),
              "editable"  => "item_id",
              "assoc" => array(
                'item_id' => array(
                    'field' => 'name',
                    'model' => 'Item'
                ),
                'item_id2' => array(
                    'field' => 'code',
                    'model' => 'Item'
                ),
                'item_name' => array(
                    'field' => 'item_name',
                    'model' => 'Item'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "displayedAs" => array(
                'total' => 'int',
                'date_in' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>