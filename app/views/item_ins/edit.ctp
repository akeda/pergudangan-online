<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ItemIn');?>
	<fieldset>
 		<legend><?php __('Edit Item In');?></legend>
        <table class="input">
            <tr>
                <td><?php echo __('Item');?>:</td>
                <td><?php echo $form->select('item_id', $items, null, array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Jumlah');?>:</td>
                <td>
                    <?php 
                        echo $form->input('total', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));
                    ?>
                    &nbsp; <span id="satuan"></span>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Date In');?>:</td>
                <td><?php echo $form->input('date_in', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['ItemIn']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', ' no ' . $this->data['Item']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->codeBlock($getSatuan);?>
<script type="text/javascript">
    $(function() {
        if ( $('#ItemInItemId').val() ) {
            $.get(getSatuan + $('#ItemInItemId').val(), function(result){
                $('#satuan').html(result);
            });
        }
        
        $('#ItemInItemId').change(function() {
            $.get(getSatuan + this.value, function(result){
                $('#satuan').html(result);
            });
        });
    });
</script>