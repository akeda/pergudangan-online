<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ItemIn');?>
	<fieldset>
 		<legend><?php __('Input Barang');?></legend>
        <table class="input">
            <tr>
                <td><?php echo __('Nama Barang');?>:</td>
                <td><?php echo $form->select('item_id', $items, null, array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Jumlah');?>:</td>
                <td>
                    <?php echo $form->input('total', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?>
                    &nbsp; <span id="satuan"></span>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Tanggal terima barang');?>:</td>
                <td><?php echo $form->input('date_in', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->codeBlock($getSatuan);?>
<script type="text/javascript">
    $(function() {
        $('#ItemInItemId').change(function() {
            $.get(getSatuan + this.value, function(result){
                $('#satuan').html(result);
            });
        });
    });
</script>