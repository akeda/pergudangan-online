<?php echo $javascript->codeBlock($getStock, false);?>
<?php echo $javascript->link('letter_numbers');?>
<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "no_start" => array(
                    'title' => __("No. Start", true), 
                    'sortable' => true
                ),
                "no_end" => array(
                    'title' => __("End", true),
                    'sortable' => true
                ),
                "valid_start" => array(
                    'title' => __("Period Begin On", true), 
                    'sortable' => false
                ),
                "valid_end" => array(
                    'title' => __("Period ended on", true),
                    'sortable' => false
                ),
                "active" => array(
                    'title' => __("Active", true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => true
                ),
                "stock" => array(
                    'title' => __("Stock", true),
                    'sortable' => false
                )
              ),
              "editable"  => "no_start",
              "assoc" => array(
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "replacement" => array(
                'active' => array(
                    1 => array(
                        'replaced' => '<strong class="green">' . __('Yes', true) . '</strong>'
                    ),
                    0 => array(
                        'replaced' => '<strong class="red">' . __('No', true) . '</strong>'
                    )
                ),
                'stock' => array(
                    '' => array(
                        'parse' => '<span class="stock" rel="%s">Loading ...</span>',
                        'model' => 'LetterNumber',
                        'field' => 'id'
                    )
                )
              ),
              "displayedAs" => array(
                'valid_start' => 'date',
                'valid_end' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>