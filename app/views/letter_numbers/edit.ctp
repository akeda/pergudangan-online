<?php pr($this->data);?>
<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('LetterNumber');?>
	<fieldset>
 		<legend><?php __('Edit Letter No.');?></legend>
        <table class="input">
            <tr>
                <td class="label-required"><?php echo __('No. From');?>:</td>
                <td>
                    <?php echo $this->data['LetterNumber']['no_start'];?>
                    <?php echo $form->hidden('no_start');?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('No. Until');?>:</td>
                <td>
                    <?php echo $this->data['LetterNumber']['no_end'];?>
                    <?php echo $form->hidden('no_end');?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Valid Date From');?>:</td>
                <td><?php echo $form->input('valid_start', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td><?php echo __('Valid Date Until');?>:</td>
                <td><?php echo $form->input('valid_end', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td><?php echo __('Active');?>:</td>
                <td><?php echo $form->input('active', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Save', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['LetterNumber']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', ' no ' . $this->data['LetterNumber']['no_start'] . ' - '. $this->data['LetterNumber']['no_end'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
