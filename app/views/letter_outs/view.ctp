<div class="<?=$this->params['controller']?> <?=$html->action?>">
	<fieldset>
 		<legend><?php echo __('View Letter Out', true) . ' : ' . $this->data[$modelName]['letter_no'];?></legend>
        <table class="input">
            <tr>
                <td class="label"><?php echo __('Letter No');?>:</td>
                <td class="val">
                    <?php 
                        echo $this->data[$modelName]['letter_no'];
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label"><?php echo __('Unit');?>:</td>
                <td class="val">
                    <?php
                        echo $this->data['UnitCode']['name'];
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label"><?php echo __('Term');?>:</td>
                <td class="val">
                    <?php 
                        echo $this->data['TermCode']['name'];
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label"><?php echo __('To');?>:</td>
                <td class="val"><?php echo $this->data[$modelName]['letter_to'];?></td>
            </tr>
            <tr>
                <td class="label"><?php echo __('Date');?>:</td>
                <td class="val"><?php echo $this->data[$modelName]['letter_date'];?></td>
            </tr>
            <tr>
                <td class="label"><?php echo __('Remark');?>:</td>
                <td class="val"><?php echo $this->data[$modelName]['remark'];?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</div>