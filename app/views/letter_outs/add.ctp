<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ItemOut');?>
	<fieldset>
 		<legend><?php __('Add Letter OUt');?></legend>
        <table class="input">
            <tr>
                <td colspan="2">
                    <?php 
                        echo $form->input('letter_no', array(
                                'div'=>false, 'label' => false, 'maxlength' => 100, 
                                'class'=>'required', 'value' => '', 'type' => 'hidden'
                            )
                        );
                        echo ($form->isFieldError('letter_no')) ? $form->error('letter_no') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label">
                    <?php 
                        echo __('Padding zero') . "<br />";
                        echo __("If you get number 1 and max. number is 9999, you'll get 0001");
                    ?>
                </td>
                <td><?php echo $form->checkbox('pad', array('div'=>false, 'label' => false, 'checked' => 'checked'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Unit');?>:</td>
                <td>
                    <?php 
                        echo $form->select('unit_code_id', $unit_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('unit_code_id')) ? $form->error('unit_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Term');?>:</td>
                <td>
                    <?php 
                        echo $form->select('term_code_id', $term_codes, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('term_code_id')) ? $form->error('term_code_id') : '';
                    ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('To');?>:</td>
                <td><?php echo $form->input('letter_to', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Date');?>:</td>
                <td><?php echo $form->input('letter_date', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Remark');?>:</td>
                <td><?php echo $form->input('remark', array('div'=>false, 'label' => false));?></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>