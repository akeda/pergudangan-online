<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "letter_no" => array(
                    'title' => __("Letter No.", true), 
                    'sortable' => false
                ),
                "unit_code_id" => array(
                    'title' => __("Unit", true),
                    'sortable' => true
                ),
                "term_code_id" => array(
                    'title' => __("Term", true),
                    'sortable' => true
                ),
                "letter_date" => array(
                    'title' => __("Date", true),
                    'sortable' => true
                ),
                "letter_to" => array(
                    'title' => __("To", true),
                    'sortable' => false
                ),
                "remark" => array(
                    'title' => __("Remark", true),
                    'sortable' => false
                ),
                "created_by" => array(
                    'title' => __("Created By", true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __("Created On", true),
                    'sortable' => true
                )
              ),
              "editable"  => "letter_no",
              "editableLink"  => "view",
              "assoc" => array(
                'unit_code_id' => array(
                    'field' => 'name',
                    'model' => 'UnitCode'
                ),
                'term_code_id' => array(
                    'field' => 'name',
                    'model' => 'TermCode'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "replacement" => array(
                'active' => array(
                    1 => array(
                        'replaced' => '<strong class="green">' . __('Yes', true) . '</strong>'
                    ),
                    0 => array(
                        'replaced' => '<strong class="red">' . __('No', true) . '</strong>'
                    )
                )
              ),
              "displayedAs" => array(
                'letter_date' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>