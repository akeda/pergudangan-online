<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('Report', array('action' => 'stocks_per_role'));?>
	<fieldset>
 		<legend><?php __('Laporan Pengguna Barang');?></legend>
        <table class="input">
            <tr id="afterThis">
                <td class="label-required"><?php echo __('Per Pengguna / Unit Kerja');?>:</td>
                <td>
                    <?php
                        $option_by = array(
                            "" => "- Select one -",
                            "p" => __("Pengguna", true), 
                            "u" => __("Unit Kerja", true)
                        );
                        echo $form->select("by", $option_by, null, array("id" => "by", "class" => "inputText"), false);
                    ?>
                </td>
            </tr>
            <tr>
                <td>Pilih Pengguna / Unit Kerja</td>
                <td id="changed"></td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Print', true), array('div'=>false, 'id' => 'print')) . '&nbsp;'; 
                    //echo __('or', true) . '&nbsp;';
                    //echo $html->link(__('Lihat grafik', true), array('action'=>'grafik'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->codeBlock($path); ?>
<?php echo $javascript->link('reports'); ?>