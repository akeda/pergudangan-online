<h1>KARTU STOK BARANG</h1>

<?php if ( !empty($info) ) {
    echo '<br />';
    echo '<span>Nama Barang: ' . $info['Item']['name'] . '</span>';
    echo '<span>Nomor Barang: ' . $info['Item']['code'] . '</span>';
    echo '<span>Satuan: ' . $info['Unit']['name'] . '</span>';
    echo '<span>Periode: ' . $time->format('d/m/Y', $info['Item']['periode']) . '</span>';
}
?>
<br />
<table align="center">
<thead>
    <tr>
        <th rowspan="2">No</th>
        <th rowspan="2">Tanggal Terima</th>
        <th colspan="2" style="text-align: center">Jumlah</th>
        <th rowspan="2">Stok</th>
        <th rowspan="2">Pengebon</th>
    </tr>
    <tr>
        <th>Penerimaan</th>
        <th>Pengeluaran</th>
    </tr>
</thead>
<tbody>
<?php
    $no = 0;
    if (!empty($items)) {
        foreach ($items as $item) {
            echo '<tr>';
            echo '<td>' . ++$no . '</td>';
            echo '<td>';
                if ( isset($item['date_in']) && !empty($item['date_in']) ) {
                    echo $time->format('d/m/Y', $item['date_in']);
                } else {
                    echo '&nbsp;';
                }
            echo '</td>';
            echo '<td>'.$item['penerimaan_periode'].'</td>';
            echo '<td>'.$item['pengeluaran'].'</td>';
            echo '<td>'.$item['stock'].'</td>';
            echo '<td>'.$item['pengebon'].'</td>';
            echo '</tr>';
        }
    } else {
        echo '<tr><td colspan="6">Tidak ada data.</td></tr>';
    }
?>
</tbody>
</table>