<div class="<?php echo $this->params['controller']; ?> index">
<?php echo $this->element('tablegrid',
        array("fields" => array(
                "item_id" => array(
                    'title' => __("Nama Barang", true),
                    'sortable' => true
                ),
                "total" => array(
                    'title' => __("Jumlah yang diminta", true),
                    'sortable' => false
                ),
                "date_out" => array(
                    'title' => __("Tgl. ingin terpenuhi", true),
                    'sortable' => false
                ),
                "date_approved" => array(
                    'title' => __("Tgl. terpenuhi", true),
                    'sortable' => true
                ),
                "total_approved" => array(
                    'title' => __("Jumlah yang terpenuhi", true),
                    'sortable' => false
                ),
                "approved" => array(
                    'title' => __("Terpenuhi/Tertunda", true),
                    'sortable' => true
                ),
                "created_by" => array(
                    'title' => __("Peminta", true),
                    'sortable' => true
                ),
                "created" => array(
                    'title' => __("Waktu input", true),
                    'sortable' => false
                )
              ),
              "editable"  => "item_id",
              "assoc" => array(
                'item_id' => array(
                    'field' => 'name',
                    'model' => 'Item'
                ),
                'created_by' => array(
                    'field' => 'name',
                    'model' => 'User'
                )
              ),
              "replacement" => array(
                'approved' => array(
                    1 => array(
                        'replaced' => '<strong class="green">' . __('Terpenuhi', true) . '</strong>'
                    ),
                    0 => array(
                        'replaced' => '<strong class="red">' . __('Belum terpenuhi', true) . '</strong>'
                    )
                ),
                // When date_approved empty
                'date_approved' => array(
                    0 => array('replaced' => '-'),
                    '' => array('replaced' => '-'),
                    null => array('replaced' => '-'),
                    '01/01/1970' => array('replaced' => '-'),
                )
              ),
              "displayedAs" => array(
                'total' => 'int',
                'total_approved' => 'int',
                'date_out' => 'date',
                'date_approved' => 'date',
                'created' => 'datetime'
              )
        ));
?>
</div>
