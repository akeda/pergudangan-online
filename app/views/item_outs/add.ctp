<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ItemOut');?>
	<fieldset>
 		<legend><?php __('Bon Barang');?></legend>
        <table class="input">
            <tr>
                <td><?php echo __('Item');?>:</td>
                <td>
                    <?php 
                        echo $form->select('item_id', $items, null, array('div'=>false, 'label' => false));
                        echo ($form->isFieldError('item_id')) ? $form->error('item_id') : '';
                    ?>
                    </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Total');?>:</td>
                <td>
                    <?php echo $form->input('total', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));?>
                    &nbsp; <span id="satuan"></span>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Tanggal ingin terpenuhi');?>:</td>
                <td><?php echo $form->input('date_out', array('div'=>false, 'label' => false, 'class' => 'inputDate'));?></td>
            </tr>
            <tr>
                <td><?php echo __('Komitmen');?>:</td>
                <td>
                <?php 
                    echo $form->input('komitmen', array('div'=>false, 'label' => false, 'type' => 'checkbox'));
                ?>
                <p>Saya akan pergunakan barang yang telah saya terima dari gudang sesuai dengan kebutuhan dan akan saya distribusikan kepada pegawai yang membutuhkan.</p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Add', true), array('div' => false, 'id' => 'add_item_outs')) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->link('item_outs');?>
<?php echo $javascript->codeBlock($getSatuan);?>
<script type="text/javascript">
    $(function() {
        $('#ItemOutItemId').change(function() {
            $.get(getSatuan + this.value, function(result){
                $('#satuan').html(result);
            });
        });
    });
</script>