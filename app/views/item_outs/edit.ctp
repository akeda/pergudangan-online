<div class="<?=$this->params['controller']?> <?=$html->action?>">
    <?php echo $form->create('ItemOut');?>
	<fieldset>
 		<legend><?php __('Lihat Data Bon Barang');?></legend>
        <table class="input">
            <tr>
                <td><?php echo __('Item');?>:</td>
                <td>
                <?php 
                    echo $form->input('item_id', array('div'=>false, 'label' => false, 'type' => 'hidden'));
                    echo $this->data['Item']['name'];
                ?>
                </td>
            </tr>
            <tr>
                <td class="label-required"><?php echo __('Total');?>:</td>
                <td>
                    <?php 
                        echo $form->input('total', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required', 'type' => 'hidden'));
                        echo $this->data['ItemOut']['total'];
                    ?>
                    &nbsp; <span class="satuan"></span>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Tgl. ingin terpenuhi');?>:</td>
                <td>
                <?php 
                    echo $form->input('date_out', array('div'=>false, 'label' => false, 'class' => 'inputDate', 'type' => 'hidden'));
                    echo $time->format('d/m/Y', $this->data['ItemOut']['date_out']);
                ?>
                </td>
            </tr>
            <tr>
                <td><?php echo __('Sudah Terpenuhi');?>:</td>
                <td>
                    <?php 
                        echo $form->input('approved', array('div'=>false, 'label' => false));
                        // echo ($this->data['ItemOut']['approved']) ? 'Terpenuhi' : 'Belum Terpenuhi';
                    ?>
                    </td>
            </tr>
            <tr id="date_approved" class="approved">
                <td><?php echo __('Tgl. terpenuhi');?>:</td>
                <td>
                <?php 
                    echo $form->input('date_approved', array('div'=>false, 'label' => false, 'class' => 'inputDate'));
                ?>
                </td>
            </tr>
            <tr id="total_approved" class="approved">
                <td class="label-required"><?php echo __('Total Terpenuhi');?>:</td>
                <td>
                    <?php 
                        echo $form->input('total_approved', array('div'=>false, 'label' => false, 'maxlength' => 100, 'class'=>'required'));
                    ?>
                    &nbsp; <span class="satuan"></span>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                <?php
                    echo $form->submit(__('Simpan', true), array('div'=>false)) . '&nbsp;' . __('or', true) . '&nbsp;';
                    echo $html->link(__('Delete', true), array('action'=>'delete', $this->data['ItemOut']['id']), array('class'=>'del'), sprintf(__('Are you sure you want to delete', true) . ' %s?', ' no ' . $this->data['Item']['name'])) . "&nbsp;" . __('or', true) . "&nbsp;";
                    echo $html->link(__('Back to index', true), array('action'=>'index'));
                ?>
                </td>
            </tr>
        </table>
	</fieldset>
</form>
</div>
<?php echo $javascript->codeBlock($getSatuan);?>
<script type="text/javascript">
    $(function() {
        $.get(getSatuan + <?php echo $this->data['Item']['id'];?>, function(result){
            $('.satuan').html(result);
        });
        
        // hide .approved related if not checked
        if ( !$('#ItemOutApproved').is(':checked') ) {
            $('.approved').hide();
            // disabled all related
            $('#ItemOutTotalApproved').attr('disabled', 'disabled');
            $('#ItemOutDateApprovedDay').attr('disabled', 'disabled');
            $('#ItemOutDateApprovedMonth').attr('disabled', 'disabled');
            $('#ItemOutDateApprovedYear').attr('disabled', 'disabled');
        }
        
        $('#ItemOutApproved').click(function() {
            if ( this.checked ) {
                $('.approved').show();
                $('#ItemOutTotalApproved').val( $('#ItemOutTotal').val() );
                $('#ItemOutTotalApproved').attr('disabled', '');
                $('#ItemOutDateApprovedDay').attr('disabled', '');
                $('#ItemOutDateApprovedMonth').attr('disabled', '');
                $('#ItemOutDateApprovedYear').attr('disabled', '');
            } else {
                $('.approved').hide();
                // disabled all related
                $('#ItemOutTotalApproved').attr('disabled', 'disabled');
                $('#ItemOutDateApprovedDay').attr('disabled', 'disabled');
                $('#ItemOutDateApprovedMonth').attr('disabled', 'disabled');
                $('#ItemOutDateApprovedYear').attr('disabled', 'disabled');
            }
        });
    });
</script>