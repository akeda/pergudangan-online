$(function() {
    /**
     * Watchdog Timer
     */
    //var watchDog = setInterval('watchDogTimer()', 5000);
});
var watchDogVars = {
        'newInputState': 0,
        'blinkState': 0
};
watchDogVars.oriColor = $('#item_outs').css('color');
var blinkTimer = 0;
function watchDogTimer() {
    $().ajaxStart(function(){
        $('#loading').hide();
    }).ajaxStop(function(){
        $('#loading').hide();
    });
    
    /*
    $.get(rootPath + 'item_outs/get_new_status', function(ret){
        if ( ret ) {
            watchDogVars.newInputState = 1;
            if ( !blinkTimer ) {
                blinkTimer = setInterval('blinkText()', 200);
            }
        } else {
            watchDogVars.newInputState = 0;
            $('#item_outs').css({'color': watchDogVars.oriColor, 'opcaity': 1});
            clearInterval(blinkTimer);
        }
    });
    */
}

function blinkText() {
    var opac = 0.2;
    if (watchDogVars.blinkState) {
        opac = 1;
    }
    $('#item_outs').css({'color': 'red', 'opacity': opac});
    watchDogVars.blinkState = !watchDogVars.blinkState
}